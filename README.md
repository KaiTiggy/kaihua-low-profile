# Kaihua Low Profile Keycap 

![shapeways render](https://images2.sw-cdn.net/product/picture/710x528_26792996_14532023_1551805459.jpg)  
[Buy it on Shapeways](https://www.shapeways.com/product/JENTQMKAT/kaihua-low-profile-keycap-primary-keys)

Blank keycaps for the Hexgears X-1 Mechanical Keyboard. 3D Print your own keys to create a custom look. Currently only have the primary square key made, but will be adding more soon. If you're looking to replicate a key, it's best to print it with "Smoothest Fine Detail Plastic" on Shapeways, and then [paint the keycap to match](https://www.shapeways.com/blog/archives/28264-how-to-make-it-a-model-train-holiday.html), masking off the shape of the letter or icon you wish to create.

Created using Autodesk Fusion 360 by someone pretending they know how to do CAD. 

See the [_issue board_](https://gitlab.com/KaiTiggy/kaihua-low-profile/boards) for supported keycap roadmap.

> Created under the terms of the Autodesk Fusion 360 Hobbyist license. Not for commercial use. 
